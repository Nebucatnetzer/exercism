def distance(strand_a, strand_b):
    if (strand_a == strand_b):
        return 0
    if (len(strand_a) != len(strand_b)):
        raise ValueError("Not the same distance")

    distance = 0
    zipped_strands = zip(strand_a, strand_b)
    for a, b in zipped_strands:
        if (a != b):
            distance += 1
    return distance
