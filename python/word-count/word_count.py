def count_words(input_sentence):
    sentence = input_sentence.lower()
    blocked_characters = [',', '.', ';', ':', '_']
    for character in blocked_characters:
        sentence = sentence.replace(character, " ")

    words = sentence.split()
    result = {}
    for word in words:
        mod_string = ""
        for elem in word:
            if elem.isalnum() or elem == "'":
                mod_string += elem
        result[mod_string] = words.count(word)

    return result
